#!/usr/bin/env python3
# pylint: disable=C0103

"""A self-hostable moderation bot based on discord.py and the Kitsuchan bot framework."""

description = ""
url = "https://gitlab.com/n303p4/channeler-bot"
version_info = ("ch", 1, 0, "Amata")
version_number = f"{version_info[0]}.{version_info[1]}.{version_info[2]}"
version = '{0}.{1}.{2} "{3}"'.format(*version_info)
