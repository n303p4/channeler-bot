#!/usr/bin/env python3

"""This extension sets the bot's playing status."""

import discord


class PlayingStatus:
    """Bot playing status manager."""

    def __init__(self, bot):
        self.bot = bot
        self.bot.loop.create_task(self.set_playing_status())

    def __unload(self):
        """If unloaded, clear playing status."""
        self.bot.loop.create_task(self.bot.change_presence(status=discord.Status.online,
                                                           activity=None))

    async def set_playing_status(self):
        """Every 10 seconds, update the bot's playing status."""
        await self.bot.wait_until_ready()

        display_prefix = self.bot.config.get('prefix', f'@{self.bot.user.name}')
        if len(display_prefix) > 1:  # No space used for single-character prefixes.
            display_prefix += " "

        status = f"Type {display_prefix}help for help!"
        game = discord.Game(name=status)
        await self.bot.change_presence(status=discord.Status.online, activity=game)


def setup(bot):
    """Set up the extension."""
    bot.add_cog(PlayingStatus(bot))
