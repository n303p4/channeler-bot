#!/usr/bin/env python3

"""Channel management commands."""

import asyncio
import discord
from discord.ext import commands


async def _archive_channel(ctx, channel):
    """Prevent anyone from viewing a channel."""
    overwrite = discord.PermissionOverwrite(read_messages=False)
    reason = f"Channel archive requested by {ctx.author} ({ctx.author.id})."

    for role in ctx.guild.roles:
        if channel.overwrites_for(role).read_messages:
            await channel.set_permissions(role, overwrite=overwrite,
                                          reason=reason)

    await ctx.send("Successfully archived channel.")


async def _clone_channel(ctx, channel):
    """Make an exact copy of a channel."""
    category = channel.category
    overwrites = dict(channel.overwrites)

    reason = (f"Clone of channel {channel} ({channel.id}) requested "
              f"by {ctx.author} ({ctx.author.id}).")

    new_channel = await ctx.guild.create_text_channel(channel.name, overwrites=overwrites,
                                                      category=category, reason=reason)

    await ctx.send(f"Successfully created clone of channel at {new_channel.mention}.")


async def _superpurge_channel(ctx, channel):
    """Delete a channel and replace it with an exact copy.

    This does not currently redirect webhooks.
    """
    name = channel.name
    category = ctx.channel.category
    overwrites = dict(channel.overwrites)

    reason_delete = f"Channel superpurge requested by {ctx.author} ({ctx.author.id})."
    reason_create = f"Replace old channel {channel} ({channel.id})."

    await ctx.channel.delete(reason=reason_delete)
    new_channel = await ctx.guild.create_text_channel(name, overwrites=overwrites,
                                                      category=category, reason=reason_create)

    message = await new_channel.send(f"Successfully purged channel.")
    await asyncio.sleep(5)
    await message.delete()


async def _remove_channel(ctx, channel):
    """Delete a channel entirely."""
    reason = f"Channel deletion requested by {ctx.author} ({ctx.author.id})."
    await channel.delete(reason=reason)


class ChannelManagement:
    """Channel management commands."""

    @commands.group(aliases=["archannel", "arc"], invoke_without_command=True)
    @commands.has_permissions(manage_roles=True)
    @commands.bot_has_permissions(manage_roles=True)
    @commands.cooldown(1, 12, commands.BucketType.channel)
    async def archivechannel(self, ctx, *, channel: discord.TextChannel):
        """Strip read message permissions from a text channel."""
        await _archive_channel(ctx, channel)

    @archivechannel.command(name=".")
    @commands.has_permissions(manage_roles=True)
    @commands.bot_has_permissions(manage_roles=True)
    @commands.cooldown(1, 12, commands.BucketType.channel)
    async def archive_current_channel(self, ctx):
        """Archive the current text channel."""
        await _archive_channel(ctx, ctx.channel)

    @commands.group(brief="Destroy and recreate a channel with no messages.",
                    aliases=["cpchannel", "cpc"], invoke_without_command=True)
    @commands.has_permissions(manage_channels=True)
    @commands.bot_has_permissions(manage_channels=True)
    @commands.cooldown(1, 12, commands.BucketType.channel)
    async def clonechannel(self, ctx, *, channel: discord.TextChannel):
        """Create a copy of a text channel with the same properties."""
        await _clone_channel(ctx, channel)

    @clonechannel.command(name=".")
    @commands.has_permissions(manage_channels=True)
    @commands.bot_has_permissions(manage_channels=True)
    @commands.cooldown(1, 12, commands.BucketType.channel)
    async def clone_current_channel(self, ctx):
        """Clone the current text channel."""
        await _clone_channel(ctx, ctx.channel)

    @commands.group(brief="Destroy and recreate a text channel with no messages.",
                    aliases=["spurge", "sp"], invoke_without_command=True)
    @commands.has_permissions(manage_channels=True)
    @commands.bot_has_permissions(manage_channels=True)
    @commands.cooldown(1, 12, commands.BucketType.channel)
    async def superpurge(self, ctx, *, channel: discord.TextChannel):
        """Destroy a text channel of your choosing and create a new one with the same properties.

        Similar to deleting all messages in a channel, but not limited by the API.

        Does NOT currently redirect webhooks; you'll have to set those up manually.
        """
        await _superpurge_channel(ctx, channel)

    @superpurge.command(name=".")
    @commands.has_permissions(manage_channels=True)
    @commands.bot_has_permissions(manage_channels=True)
    @commands.cooldown(1, 12, commands.BucketType.channel)
    async def superpurge_current_channel(self, ctx):
        """Superpurge the current text channel."""
        await _superpurge_channel(ctx, ctx.channel)

    @commands.group(aliases=["rmchannel", "rmc"], invoke_without_command=True)
    @commands.has_permissions(manage_channels=True)
    @commands.bot_has_permissions(manage_channels=True)
    @commands.cooldown(1, 12, commands.BucketType.channel)
    async def removechannel(self, ctx, *, channel: discord.TextChannel):
        """Delete a text channel of your choosing."""
        await _remove_channel(ctx, channel)

    @removechannel.command(name=".")
    @commands.has_permissions(manage_channels=True)
    @commands.bot_has_permissions(manage_channels=True)
    @commands.cooldown(1, 12, commands.BucketType.channel)
    async def remove_current_channel(self, ctx):
        """Delete the current text channel."""
        await _remove_channel(ctx, ctx.channel)


def setup(bot):
    """Set up the extension."""
    bot.add_cog(ChannelManagement())
