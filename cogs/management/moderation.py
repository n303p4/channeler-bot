#!/usr/bin/env python3

"""Moderation commands."""

import asyncio
from discord.ext import commands
from kitsuchan import helpers

STATUS_INDICATORS = {"online": ":green_heart:",
                     "idle": ":yellow_heart:",
                     "dnd": ":heart:",
                     "offline": ":black_heart:"}


def _get_mods(ctx):
    mods = []
    for member in ctx.guild.members:
        if (ctx.channel.permissions_for(member).manage_messages and
                ctx.channel.permissions_for(member).kick_members and
                not member.bot):
            mods.append(member)
    return mods


class Moderation:
    """Moderation commands."""

    @commands.command()
    @commands.has_permissions(kick_members=True)
    @commands.bot_has_permissions(kick_members=True)
    @commands.cooldown(1, 6, commands.BucketType.channel)
    async def kick(self, ctx, *, member_and_reason: str):
        """Kick a specific user.

        You may separate the member and reason with a |."""
        if "|" in member_and_reason:
            member_and_reason = member_and_reason.split("|")
            member = await helpers.member_by_substring(ctx, member_and_reason[0].strip())
            reason = f"{ctx.author} ({ctx.author.id}): {member_and_reason[-1].strip()}"
        else:
            member = await helpers.member_by_substring(ctx, member_and_reason)
            reason = None
        await ctx.guild.kick(member, reason=reason)
        await ctx.send(f"Kicked {member}.")

    @commands.command()
    @commands.has_permissions(kick_members=True)
    @commands.bot_has_permissions(kick_members=True)
    @commands.cooldown(1, 12, commands.BucketType.channel)
    async def masskick(self, ctx):
        """Kick all users mentioned by this command.

        Requires both the user and bot to have `kick_members` to execute.
        """
        if not ctx.message.mentions:
            raise commands.UserInputError("Please mention some members to kick.")

        reason = await helpers.input_text(ctx, "Please enter a reason for the kick.",
                                          timeout=30)
        reason = f"{ctx.author} ({ctx.author.id}): {reason}"

        for member in ctx.message.mentions:
            await ctx.guild.kick(member, reason=reason)

        await ctx.send("Kicked the requested members.")

    @commands.command()
    @commands.has_permissions(kick_members=True)
    @commands.bot_has_permissions(kick_members=True)
    @commands.cooldown(1, 6, commands.BucketType.channel)
    async def ban(self, ctx, *, member_and_reason: str):
        """Ban a specific user.

        You may separate the member and reason with a |."""
        if "|" in member_and_reason:
            member_and_reason = member_and_reason.split("|")
            member = await helpers.member_by_substring(ctx, member_and_reason[0].strip())
            reason = f"{ctx.author} ({ctx.author.id}): {member_and_reason[-1].strip()}"
        else:
            member = await helpers.member_by_substring(ctx, member_and_reason)
            reason = None
        await ctx.guild.ban(member, reason=reason)
        await ctx.send(f"Banned {member}.")

    @commands.command()
    @commands.has_permissions(ban_members=True)
    @commands.bot_has_permissions(ban_members=True)
    @commands.cooldown(1, 12, commands.BucketType.channel)
    async def massban(self, ctx):
        """Ban all users mentioned by this command.

        Requires both the user and bot to have `ban_members` to execute.
        """
        if not ctx.message.mentions:
            raise commands.UserInputError("Please mention some members to ban.")

        reason = await helpers.input_text(ctx, "Please enter a reason for the ban.", timeout=30)
        reason = f"{ctx.author} ({ctx.author.id}): {reason}"

        for member in ctx.message.mentions:
            await ctx.guild.ban(member, reason=reason)

        await ctx.send("Banned the requested members.")

    @commands.command(aliases=["prune"])
    @commands.has_permissions(manage_messages=True)
    @commands.bot_has_permissions(manage_messages=True)
    @commands.cooldown(1, 12, commands.BucketType.channel)
    async def purge(self, ctx, limit: int):
        """Purge messages from the current channel.

        Can only purge up to 2 week's worth of messages, due to API limitations.

        Requires both the user and bot to have `manage_messages` to execute.
        """
        limit += 1
        message_clumps = limit // 100
        leftover_messages = limit % 100
        for _ in range(0, message_clumps):
            await ctx.channel.purge(limit=100)
            await asyncio.sleep(5)
        await ctx.channel.purge(limit=leftover_messages)

    @commands.command(aliases=["moderators"])
    @commands.cooldown(1, 12, commands.BucketType.channel)
    async def mods(self, ctx):
        """Display moderators for the given channel.

        Assumes that members with `manage_messages`, `kick_members`, and `ban_members` are mods.
        """
        mods = _get_mods(ctx)
        message = ["**__Moderators__**"]
        for status in STATUS_INDICATORS:
            emote = STATUS_INDICATORS[status]
            mods_with_status = []
            for mod in mods:
                if mod.status.name == status:
                    mods_with_status.append(f"**{mod.name}**#{mod.discriminator}")
            if mods_with_status:
                message.append(f"{emote} " + ", ".join(mods_with_status))
        message = "\n".join(message)
        await ctx.send(message)

    @commands.command()
    @commands.has_permissions(manage_nicknames=True)
    @commands.bot_has_permissions(manage_nicknames=True)
    @commands.cooldown(6, 12)
    async def dehoist(self, ctx, *, member: str):
        """Add a z to the front of someone's username."""
        member = await helpers.member_by_substring(ctx, member)
        await member.edit(nick="z" + member.display_name)
        await ctx.send("Yoink!")


def setup(bot):
    """Set up the extension."""
    bot.add_cog(Moderation())
