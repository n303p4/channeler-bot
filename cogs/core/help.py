#!/usr/bin/env python3

"""A butchered help command."""

import discord
from discord.ext import commands


def embed_from_dict(dictionary):
    """Convert a specially crafted dict into an embed."""
    embed = discord.Embed()
    for key, value in sorted(dictionary.items(), key=lambda x: x[0]):
        if isinstance(value, list):
            value = sorted(value)
            value = ", ".join(value)
        embed.add_field(name=key, value=value, inline=False)
    return embed


class Help:
    """Help command."""

    @commands.command(aliases=["commands"])
    @commands.cooldown(1, 2)
    async def help(self, ctx, *args: str):
        """Help command."""
        if not args:
            command_categories = {}
            for command in ctx.bot.commands:
                if command.hidden or not command.instance:
                    continue
                try:
                    can_run = await command.can_run(ctx)
                except commands.CommandError:
                    continue
                if not can_run:
                    continue
                module_name = command.instance.__module__.split(".")
                command_category = (module_name[1] if len(module_name) > 1 else
                                    command.instance.__class__)
                command_category = command_category.capitalize()
                if not command_category in command_categories:
                    command_categories[command_category] = []
                command_categories[command_category].append(command.name)
                if len(command.aliases) > 10:
                    command_categories[command.name.capitalize()] = command.aliases
            embed = embed_from_dict(command_categories)
            embed.description = (f"Use `{ctx.prefix}help <command_name>` "
                                 "for more details on a command!")
            await ctx.send(embed=embed)
        else:
            await ctx.bot.all_commands["old_help"].callback(ctx, *args)


def setup(bot):
    """Set up the extension."""
    if not "old_help" in bot.all_commands:
        help_command = bot.all_commands["help"]
        help_command.hidden = True
        help_command.name = "old_help"
        bot.remove_command("help")
        bot.add_command(help_command)
    bot.add_cog(Help())
