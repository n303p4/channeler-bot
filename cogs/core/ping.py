#!/usr/bin/env python3

"""Ping command for a discord.py rewrite bot."""

from discord.ext import commands


class Ping:
    """Ping command."""

    @commands.command()
    @commands.cooldown(6, 12)
    async def ping(self, ctx):
        """Ping the bot."""
        hb_latency = round(ctx.bot.latency*1000, 2)
        if ctx.guild and len(ctx.bot.latencies) > 1:
            current_shard_latency = ctx.bot.latencies[ctx.guild.shard_id]
            shb_latency = round(current_shard_latency[1]*1000, 2)
            message_content = (f":heartbeat: Heartbeat latency: {hb_latency} ms\n"
                               f":broken_heart: Current shard heartbeat: {shb_latency} ms")
        else:
            message_content = f":heartbeat: Heartbeat latency: {hb_latency} ms"
        ping_msg = await ctx.send(message_content)
        rt_ping = round((ping_msg.created_at - ctx.message.created_at).total_seconds() * 1000,
                        2)
        message = f":revolving_hearts: Round-trip time: {rt_ping} ms\n{message_content}"
        await ping_msg.edit(content=message)


def setup(bot):
    """Set up the extension."""
    bot.add_cog(Ping())
